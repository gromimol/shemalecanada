$(document).ready(function () {
    $('.main-slider').slick({
        appendArrows: '.main-slider__arrows',
        prevArrow: '<span class="prev-slide"><svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.237663 5.4197L5.38367 0.23997C5.70154 -0.0799899 6.21135 -0.0799899 6.52922 0.23997C6.8471 0.55993 6.8471 1.07307 6.52922 1.39303L2.76268 5.18425H19.1903C19.6401 5.18425 20 5.54647 20 5.99924C20 6.45202 19.6401 6.81424 19.1903 6.81424H2.76268L6.52922 10.6055C6.8471 10.9254 6.8471 11.4386 6.52922 11.7585C6.37328 11.9155 6.16337 12 5.95944 12C5.75552 12 5.5456 11.9215 5.38967 11.7585L0.243658 6.57879C-0.0802155 6.2528 -0.0802135 5.73362 0.237663 5.4197Z" fill="currentColor"</svg></span>',
        nextArrow: '<span class="next-slide"><svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.7623 5.4197L14.6163 0.23997C14.2985 -0.0799899 13.7887 -0.0799899 13.4708 0.23997C13.1529 0.55993 13.1529 1.07307 13.4708 1.39303L17.2373 5.18425H0.809686C0.35986 5.18425 0 5.54647 0 5.99924C0 6.45202 0.35986 6.81424 0.809686 6.81424H17.2373L13.4708 10.6055C13.1529 10.9254 13.1529 11.4386 13.4708 11.7585C13.6267 11.9155 13.8366 12 14.0406 12C14.2445 12 14.4544 11.9215 14.6103 11.7585L19.7563 6.57879C20.0802 6.2528 20.0802 5.73362 19.7623 5.4197Z" fill="currentColor"/></svg></span>',
        autoplay: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false
                }
            }
        ],
    })
    $('.slider-preview').slick({
        variableWidth: true,
        swipeToSlide: true,
        prevArrow: '<span class="prev-slide"><svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.237663 5.4197L5.38367 0.23997C5.70154 -0.0799899 6.21135 -0.0799899 6.52922 0.23997C6.8471 0.55993 6.8471 1.07307 6.52922 1.39303L2.76268 5.18425H19.1903C19.6401 5.18425 20 5.54647 20 5.99924C20 6.45202 19.6401 6.81424 19.1903 6.81424H2.76268L6.52922 10.6055C6.8471 10.9254 6.8471 11.4386 6.52922 11.7585C6.37328 11.9155 6.16337 12 5.95944 12C5.75552 12 5.5456 11.9215 5.38967 11.7585L0.243658 6.57879C-0.0802155 6.2528 -0.0802135 5.73362 0.237663 5.4197Z" fill="currentColor"</svg></span>',
        nextArrow: '<span class="next-slide"><svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.7623 5.4197L14.6163 0.23997C14.2985 -0.0799899 13.7887 -0.0799899 13.4708 0.23997C13.1529 0.55993 13.1529 1.07307 13.4708 1.39303L17.2373 5.18425H0.809686C0.35986 5.18425 0 5.54647 0 5.99924C0 6.45202 0.35986 6.81424 0.809686 6.81424H17.2373L13.4708 10.6055C13.1529 10.9254 13.1529 11.4386 13.4708 11.7585C13.6267 11.9155 13.8366 12 14.0406 12C14.2445 12 14.4544 11.9215 14.6103 11.7585L19.7563 6.57879C20.0802 6.2528 20.0802 5.73362 19.7623 5.4197Z" fill="currentColor"/></svg></span>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false
                }
            }
        ],
    })

    // open dropdown
    $('.js--dropdown-link').on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('active').find('.js--dropdown').slideToggle()
    })

    // slider
    $('.banners-slider').slick({
        prevArrow: '<span class="prev-slide arrow-black"><svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M0.237663 5.4197L5.38367 0.23997C5.70154 -0.0799899 6.21135 -0.0799899 6.52922 0.23997C6.8471 0.55993 6.8471 1.07307 6.52922 1.39303L2.76268 5.18425H19.1903C19.6401 5.18425 20 5.54647 20 5.99924C20 6.45202 19.6401 6.81424 19.1903 6.81424H2.76268L6.52922 10.6055C6.8471 10.9254 6.8471 11.4386 6.52922 11.7585C6.37328 11.9155 6.16337 12 5.95944 12C5.75552 12 5.5456 11.9215 5.38967 11.7585L0.243658 6.57879C-0.0802155 6.2528 -0.0802135 5.73362 0.237663 5.4197Z" fill="currentColor"/>\n' +
            '</svg>\n</span>',
        nextArrow: '<span class="next-slide arrow-black"><svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M19.7623 5.4197L14.6163 0.23997C14.2985 -0.0799899 13.7887 -0.0799899 13.4708 0.23997C13.1529 0.55993 13.1529 1.07307 13.4708 1.39303L17.2373 5.18425H0.809686C0.35986 5.18425 0 5.54647 0 5.99924C0 6.45202 0.35986 6.81424 0.809686 6.81424H17.2373L13.4708 10.6055C13.1529 10.9254 13.1529 11.4386 13.4708 11.7585C13.6267 11.9155 13.8366 12 14.0406 12C14.2445 12 14.4544 11.9215 14.6103 11.7585L19.7563 6.57879C20.0802 6.2528 20.0802 5.73362 19.7623 5.4197Z" fill="currentColor"/>\n' +
            '</svg>\n</span>',
        dots: true
    })
    // open search in header
    $('html').on('click', function () {
        $('.header').removeClass('open-search')
        $(".search-input").val('')
        $('.user-header__body').removeClass('active')
        $('.dropdown').hide();
    })
    $(".header").on("click", function (e) {
        e.stopPropagation()
    })
    $('.js--open-search').on('click', function (e) {
        e.preventDefault()
        $(this).closest(".header").toggleClass("open-search")
        if ($('.header').hasClass('open-search')) {
            $(this).find('svg use').attr('xlink:href', '#svg-close')
        } else {
            $(this).find('svg use').attr('xlink:href', '#svg-search')
        }
    })

    // mobile menu
    $(".burger").on("click", function () {
        $('body').addClass('noscroll')
        $(".header__nav").addClass('active')
    })
    $('.close-menu').on('click', function () {
        $('body').removeClass('noscroll')
        $(".header__nav").removeClass('active')
    })

    // open submenu on mobile
    $('.submenu__parent > a').on('click', function (e) {
        if($(window).width() < 1271) {
            e.preventDefault();
            $(this).parent('li').toggleClass('active')
            $(this).next(".submenu").slideToggle()
        }
    })

    // demo. Add like
    $('.raiting .icon').on('click', function () {
        $(this).toggleClass('active')
    })

    // tabs
    $('.tab-list').on('click', 'li:not(.active)', function() {
        $(this).addClass('active').siblings().removeClass('active')
        $('.tab-content').removeClass('active').eq($(this).index()).addClass('active');
    });

    // user dropdown in header
    $('.user-header__body').on('click', function (){
        $(this).toggleClass('active').next('.dropdown').toggle();
    })
})
